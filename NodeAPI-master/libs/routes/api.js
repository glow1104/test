var express = require('express');
var passport = require('passport');
var router = express.Router();

/* GET users listing. */
router.get('/', function (req, res) {
    res.send({ data: "data", pageSize: "pageSize", pageCount: "pageCount",currentPage: "currentPage"});
});

module.exports = router;
