var express    = require('express');
var router     = express.Router();
var MongoClient = require('mongodb').MongoClient;
var url = "mongodb://localhost:27017/apiDB";

var totalRec = 0;
var pageSize  = 6;
var pageCount = 0;
var start = 0;
var currentPage = 1;
var arr=""; 
router.get('/', function(req, res, next) {
	MongoClient.connect(url, function(err, db) {
		if (typeof req.query.page !== 'undefined') {
	        currentPage = req.query.page;
	    }
		if(currentPage >=1 ){
	        start = (currentPage - 1) * pageSize;
	    }
	    console.log(start);

		if (err) throw err;
		db.collection("customers").find({}).toArray(function(err, result) {
			if (err) throw err;
			totalRec=result.length;
		});
		pageCount =  Math.ceil(totalRec /  pageSize);		             
		db.collection("customers").find().limit( 6 ).skip( start ).toArray(function(err, result) {
			if (err) throw err;
			arr=result;
			if (typeof req.query.page !== 'undefined') 
			{
			  res.send({ data: arr, pageSize: pageSize, pageCount: pageCount,currentPage: currentPage});
			} 
			else 
			{
			  res.render('index');
			}						
		});
	});
});
module.exports = router;