var index=12121;
$(document).ready(function(){   
     if (index==1) {$('.pre').hide();}
     first();
})

function first() {
    var url='/?page=1';
    $.ajax({
        url: url,
        type: 'get',   
    })
    .done(function(data) {
    var q=data.data;
    draw_table(q,1);  
    })
}

function next() {
    index++;
    var url='/?page='+index;
    $.ajax({
        url: url,
        type: 'get',   
    })
    .done(function(data) {
    var q=data.data;
    draw_table(q,data.currentPage);
    if ((index+1)>data.pageCount) {
        $('.next').hide();
    }     
    $('.pre').show();    
    })
}

function pre() {
    index--;
    if (index==1) {$('.pre').hide();}
    var url='/?page='+index;
    $.ajax({
        url: url,
        type: 'get',   
    })
    .done(function(data) {
    var q=data.data;
    draw_table(q,data.currentPage);
    $('.next').show();
    })
}

function draw_table(qq,page) {
    $('tbody').remove();
    $('table').append('<tbody><tr><th>ID</th><th>Name</th><th>Password</th></tr>');
    $.each(qq, function(i,value){
        var No=(page-1)*6+i+1;
        $('table').append("<tr><td>"+No+"</td><td>"+value['name']+"</td><td>"+value['password']+"</td></tr>");
    });
}